export const state = () => ({
  flicker: true,
  popups: true,
  toggle: false,
})

export const mutations = {
  TOGGLE_FLICKER(state) {
    state.flicker = !state.flicker
  },
  TOGGLE_POPUPS(state) {
    state.popups = !state.popups
  },
  TOGGLE_OPTIONS(state) {
    state.toggle = !state.toggle
  },
}

export const actions = {}
