import projectsData from '~/assets/data/projects.json'

export const state = () => ({
  selected: {},
  selectedBool: false,
  collection: [],
})

export const mutations = {
  select_project(state, selected) {
    state.selected = selected
    state.selectedBool = true
  },
  populate_projects(state, projects) {
    state.collection = []
    state.collection = projects
  },
}

export const actions = {
  populate({ commit }) {
    commit('populate_projects', projectsData)
  },
}
