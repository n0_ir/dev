export const state = () => ({
  loggedIn: false,
  accountLoggedIn: {},
})

export const mutations = {
  LOG_IN(state, account) {
    state.loggedIn = true

    state.accountLoggedIn = account
  },
  LOG_OUT(state) {
    state.loggedIn = false
    state.accountLoggedIn = {}
    this.app.router.push('/directory')
  },
}
