export const state = () => ({
  notification: {},
})

export const mutations = {
  SET_NOTIFICATION_MESSAGE(state, notification) {
    state.notification = {
      text: notification.text,
      colour: notification.colour,
    }
  },
  RESET_NOTIFICATION_MESSAGE(state) {
    state.notification = {}
  },
}
